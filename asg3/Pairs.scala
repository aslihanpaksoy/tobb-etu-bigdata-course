val rdd = sc.textFile("100.txt")

var lineCount=155213


val pair = rdd.map{ 

    _.split('.').map{ substrings =>

        //bosluklara gore ayir
        substrings.trim.split(' ').

		map{_.toLowerCase.replaceAll("""[^\p{IsAlphabetic}]+""","")}. //

        // bigramları bul
        sliding(2)
    }.


    flatMap{identity}.map{_.mkString(" ")}.

    //bigramlarin frekansi lazim
    groupBy{identity}.mapValues{_.size}

}.flatMap{identity}.reduceByKey(_+_).sortByKey()


val wordCollection = rdd.flatMap(line => line.toLowerCase.replaceAll("(^[^a-z]+|[^a-z]+$)",	"").split(" "))
val wordCounts = wordCollection.map(x => (x, 1)).reduceByKey(_+_).sortByKey()


for((x,y) <- pair)
{
	val keys=x.split(" ")
	val cntX=wordCounts.lookup(keys(0))
	 
	val px=cntX(0) / lineCount
	
	val cntY=wordCounts.lookup(keys(1))
	val py=cntY(1) / lineCount
	
	var pxy =(y/lineCount) 
	var pmi=scala.math.log(px*py)/scala.math.log(pxy) -1
	
	y -> pmi
}

pair.saveAsTextFile("output")
