Hocam deneye yanıla, internetten de araştırarak bu kadar yapabildim. fakat ne yaptıysam çalıştıramadım hadoopta. Dolayısıyla yüklediğim java kodlarından da emin değilim, ama implement ederken aşağıdakileri yapmaya çalıştım.


Q1)


I use "pairs" and "stripes" implementation. For "pairs", each map emits all unique ordered word pairs and (word ), each reducers then sum up total unique pairs, and number of occurrence of each word (sum of (word *)), I tried a trick on reducer which write out, for example, if we have Count(A,B), Count(A *) then I will write out 2 pairs-values, one of which is in reverse order. By doing this trick, the final output will contains every ordered pairs 2 times. Example: (A,B) will have 2 values , 1 for Log(Count(A,B))+Log(Total)-Log(Count(B)), and 1 for -Log(Count(A,*)). I used the second MR job to simply just add those two values to get PMI(A,B). The final output record will be (A,B) PMI(A,B).

For "stripes", similar to the approach I used for pairs, the only difference is that I used "stripes" strategy in which a mapper emits a word and all associative words with value of 1, regardless of order. For example, a sentence A B C, the mapper emits A->*:1,B:1,C:1.

